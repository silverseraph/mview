## mview - Medical Viewer 

### Summary

The purpose of this is to act as a lightweight alternative to cview or slicer.  Visuals are implemented in gtk+ and and c.  This currently supports both GTK2+ and GTK3+ and allows the user to decide between them.  All visuals thus far are simple, as this isn't meant to be a full alternative to Slicer.  That would be pointless and a waste of time.

### Features

1. Allows the user to open multiple images at the same time, and uses a notebook to view the images.
2. Allows the user to diff images without the need of an external program.
3. Caches images so that each image is only opened a single time. 
4. Provides a Lua scripting interface to manipulate mview's functionality.  Lua script is handled as if by the Lua interpreter, which enables the user to write single blocks of code over multiple lines.

