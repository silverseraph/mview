using Gtk;

public class MviewGroup : GLib.Object
{
    //public MviewTab label;
    public MviewPage page;
    public Notebook parent;

    public MviewGroup(Notebook parent, Volume vol, string name)
    {
        this.parent = parent;
        //this.label = new MviewTab(this, name);
        //this.page = new MviewPage(file_name);
        this.page = new MviewPage(vol);

        //this.idx = this.parent.append_page(this.page, this.label);
#if GTK2
        var button = new Button.from_stock("STOCK_CLOSE");
#else
        var button = new Button.from_icon_name("window-close", IconSize.BUTTON);
#endif
        var label = new Label(name);
        button.clicked.connect(() =>
                {
                this.close();
                });

        this.parent.append_page(this.page, label);
        //this.parent.append_page(this.page, button);
    }

    ~MviewGroup()
    {
        this.parent = null;
        this.page = null;
    }

    public void close()
    {
        var idx = this.parent.page_num(this.page);

        this.parent.remove_page(idx);
    }
}
