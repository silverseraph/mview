#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>

#include <glib.h>

#include "mview_utils.h"

bool DEBUG = false;

void mview_page_render(guint8 *dst, long int *dim, float *vol, float min_threshold, float max_threshold, long int slice_idx)
{
    int stride = (int)dim[0];
    int x, y;

#ifdef USE_OMP
    #pragma omp parallel for \
        default(none) \
        private(x, y) \
        shared(min_threshold, max_threshold, dst, dim, slice_idx, vol, stride) \
        collapse(2)
#endif
    for(y = 0; y < dim[1]; y++)
    {
        for(x = 0; x < dim[1]; x++)
        {
            long int idx = ((stride * y) + x) * 3;
            float fval = vol[VOL_IDX(x, y, slice_idx, dim)];
            if(fval < min_threshold) fval = 0.0f;
            if(fval > max_threshold) fval = 0.0f;
            guint8 pval = (guint8)(fval * 255.0f);
            dst[idx + 0] = pval;
            dst[idx + 1] = pval;
            dst[idx + 2] = pval;
        }
    }
}
