using Gtk;
using Gdk;

public class MviewWindow : Gtk.Window
{
    private Notebook nb;
    private Vala.HashMap<string, MviewGroup> groups;

    public MviewWindow()
    {
        GLib.Object();
        this.nb = new Notebook();
        this.add(this.nb);
        this.groups = new Vala.HashMap<string, MviewGroup>(str_hash, str_equal);

        this.key_press_event.connect(this.key_press);
    }

    public bool open_mha(string file_name, string? tab_name = null)
    {
        if(!FileUtils.test(file_name, FileTest.EXISTS)) 
        {
            stderr.printf("MviewWindow.open_mha: Unable to find file \"%s\".\n", file_name);
            return false;
        }
        var vol = new Volume.from_mha(file_name);

        var g = new MviewGroup(this.nb, vol, tab_name ?? file_name);

        this.groups[tab_name ?? file_name] = g;

        push_frame(tab_name ?? file_name, g);

        return true;
    }

    private bool key_press(EventKey e)
    {
        switch(e.str)
        {
            case "1":
                break;
            case "2":
                break;
            case "3":
                break;
            default:
#if DEBUG
                stdout.printf("Uncaught key: %s\n", e.str);
#endif
            break;
        }

        return false;
    }

    public void open_volume(string tab_name, Volume vol)
    {
        var g = new MviewGroup(this.nb, vol, tab_name);

        this.groups[tab_name] = g;

        push_frame(tab_name, g);
    }

    public void refresh_group(string name)
    {
        var group = this.groups[name];

        if(group != null)
        {
            group.page.redraw();
        }
    }
}
