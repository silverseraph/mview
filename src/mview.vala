using Lua;
using Readline;

/* global variables */
LuaVM lua;
MviewWindow window;
Vala.HashMap<string, VolumeData> cache;

[CCode(cname="sleep_if_not_tty")]
extern void sleep_if_not_tty();

int main(string[] args)
{
    lua = new LuaVM();
    lua.open_libs();
    handle_luainit(lua);
    register_functions();

    Gtk.init(ref args);

    window = new MviewWindow();

    window.set_default_size(800, 600);
    window.title = "Mview - MHA Image Viewer";
    window.border_width = 10;
    window.destroy.connect(() => 
    {
        stdout.write("\n".data);

        Process.raise(ProcessSignal.KILL);
    });

    window.show_all();

    if(args.length < 2)
    {
        log_warn("No volumes provided.");
        show_help(args[0]);
    }

    cache = new Vala.HashMap<string, VolumeData>(str_hash, str_equal);
    Vala.ArrayList<Order> orders = new Vala.ArrayList<Order>();

    VolumeData temp_arg1 = null;
    VolumeData temp_arg2 = null;

    for(int i = 1; i < args.length; i++)
    {
        switch(args[i])
        {
            case "-h":
            case "--help":
                show_help(args[0]);
                return 0;
            case "-d":
            case "--diff":
                i += 2;
                if(i < args.length)
                {
                    temp_arg1 = new VolumeData.from_arg(args[i-1]);
                    temp_arg2 = new VolumeData.from_arg(args[i]);

                    if(cache.contains(temp_arg1.path) || cache.contains(temp_arg1.alias)) cache[temp_arg1.alias].cache_ref();
                    if(cache.contains(temp_arg2.path) || cache.contains(temp_arg2.alias)) cache[temp_arg2.alias].cache_ref();

                    if(!cache.contains(temp_arg1.path)) cache[temp_arg1.path] = temp_arg1;
                    if(!cache.contains(temp_arg1.alias)) cache[temp_arg1.alias] = temp_arg1;
                    if(!cache.contains(temp_arg2.path)) cache[temp_arg2.path] = temp_arg2;
                    if(!cache.contains(temp_arg2.alias)) cache[temp_arg2.alias] = temp_arg2;

                    orders.add(new Order(CommandName.DIFF, new string[]{temp_arg1.alias.dup(), temp_arg2.alias.dup()}));
                }
                break;
            default:
                temp_arg1 = new VolumeData.from_arg(args[i]);
                if(cache.contains(temp_arg1.path) || cache.contains(temp_arg1.alias)) cache[temp_arg1.alias].cache_ref();
                else
                {
                    cache[temp_arg1.path] = temp_arg1;
                    cache[temp_arg1.alias] = temp_arg1;
                }
                orders.add(new Order(CommandName.VISUAL, new string[]{temp_arg1.alias.dup()}));
                break;
        }
    }

    foreach(var vd in cache.get_values())
    {
        vd.load();
    }

    foreach(var order in orders)
    {
        order.execute(window, cache);
    }

    foreach(var vd in cache.get_values())
    {
        push_volume(vd.alias, vd);
    }

    Thread<void*> lua_thread = new Thread<void*>("lua_thread", () =>
    {
        string name_line = "Mview LUA <%s> interface\n".printf(Lua.RELEASE);
        string name_wrap = string.nfill(name_line.length, '-');
        name_wrap.data[name_line.length-1] = '\n';

        stdout.puts(name_wrap);
        stdout.puts(name_line);
        stdout.puts(name_wrap);

        dotty(lua);

        sleep_if_not_tty();

        exit(1);

        return null;
    });

    unused<Thread<void*>>(lua_thread);

    Gtk.main();

    return 0;
}

public class VolumeData
{
    public string alias;
    public string path;
    public Volume volume;
    public int rc;

    public VolumeData.from_arg(string arg)
    {
        string[] tokens = arg.split(",");

        this.path = tokens[0];
        if(tokens.length > 1) this.alias = tokens[1];
        else                  this.alias = this.path;
        this.volume = null;
        this.rc = 1;
    }

    public VolumeData.with_data(string path, Volume v, string? alias = null)
    {
        //this.alias = alias ?? path;
        if(alias == null) this.alias = path;
        else              this.alias = alias;
        this.path = path;
        this.volume = v;
        this.rc = 1;
    }

    public void load()
    {
        if(this.volume == null)
        {
            assert(this.path != null);
            this.volume = new Volume.from_mha(this.path);
        }
    }

    public void cache_unref()
    {
        this.rc--;
    }

    public void cache_ref()
    {
        this.rc++;
    }
}

public class Order
{
    public CommandName cmd;
    public string[] volumes;

    public Order(CommandName cmd, string[] volumes)
    {
        this.cmd = cmd;
        this.volumes = volumes;
    }

    public void execute(MviewWindow window, Vala.HashMap<string, VolumeData> cache)
    {
        switch(this.cmd)
        {
            case CommandName.VISUAL:
                assert(volumes[0] != null);
                var vd = cache[volumes[0]];
                assert(vd != null);
                window.open_volume(volumes[0], vd.volume);
                window.show_all();
                break;
            case CommandName.DIFF:
                string name = "d(%s,%s)".printf(volumes[0], volumes[1]);
                Volume d = cache[volumes[0]].volume.meta_clone();
                Volume.diff(d, cache[volumes[0]].volume, cache[volumes[1]].volume);
                cache[volumes[0]].cache_unref();
                if(cache[volumes[0]].rc < 1) cache.remove(volumes[0]);
                cache[volumes[1]].cache_unref();
                if(cache[volumes[1]].rc < 1) cache.remove(volumes[1]);
                cache[name] = new VolumeData.with_data(name, d, name);
                window.open_volume(name, d);
                window.show_all();
                break;
            default:
                stdout.printf("Command is currently unhandled.\n");
                break;
        }
    }
}

public enum CommandName
{
    VISUAL,
    LANDMARK,
    DIFF,
    INVALID
}

void show_help(string name)
{
    stdout.printf("\tUsage: %s [--diff|-d [file1] [file2]] [files...]\n", name);
    stdout.printf("\t          -d | --diff  Diff the two files, and set the result into a new tab.\n");
    stdout.printf("\t          -h | --help  Show the help dialog\n");
}
