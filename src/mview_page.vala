using Gtk;
using Gdk;

public class MviewPage : Gtk.EventBox
{
    [CCode(cname="mview_page_render", has_target="false")]
    private static extern void render_page(uint8 *dst, long *dim, float *vol, float min_threshold, float max_threshold, long slice_idx);

    /* MEMBERS */
    public Volume vol;
    public Gtk.Image img;

    public ViewRotation rot
    {
        get
        {
            return this._rot;
        }
    }

    public long z 
    {
        get
        {
            return this._z;
        }
        set
        {
            this._z = value;

            if(this._z < 0) this._z = 0;

            switch(this._rot)
            {
                case ViewRotation.AXIAL:
                    if(this._z >= this.vol.dim[2]) this._z = (this.vol.dim[2] - 1);
                    break;
                case ViewRotation.CORONAL:
                    if(this._z >= this.vol.dim[1]) this._z = (this.vol.dim[1] - 1);
                    break;
                case ViewRotation.SAGITTAL:
                    if(this._z >= this.vol.dim[0]) this._z = (this.vol.dim[0] - 1);
                    break;
                default:
                    this._z = 0;
                    break;
            }
        }
    }

    public bool invert
    {
        get
        {
            return this._invert;
        }
        set
        {
            bool prev = this._invert;
            this._invert = value;
            if(prev != value) this.redraw();
        }
    }

    public float min_threshold
    {
        get
        {
            return this._min_threshold;
        }
        set
        {
            this._min_threshold = value;
            if(this._min_threshold < 0f)
                this._min_threshold = 0f;
            if(this._min_threshold > 1f)
                this._min_threshold = 1f;
            if(this._min_threshold > this._max_threshold)
                this._min_threshold = this._max_threshold;

            this.redraw();
        }
    }

    public float max_threshold
    {
        get
        {
            return this._max_threshold;
        }
        set
        {
            this._max_threshold = value;
            if(this._max_threshold < 0f)
                this._max_threshold = 0f;
            if(this._max_threshold > 1f)
                this._max_threshold = 1f;
            if(this._max_threshold < this._min_threshold)
                this._max_threshold = this._min_threshold;

            this.redraw();
        }
    }

    /* PRIVATE VARIABLES */
    private long _z;
    private ViewRotation _rot;
    private bool _invert;
    private int _width;
    private int _height;
    private float _min_threshold;
    private float _max_threshold;
    private bool _paused;
    private bool _pending;
    private bool _drawing;

    public MviewPage(Volume vol)
    {
        GLib.Object();
        this.vol = vol;
        this._rot = ViewRotation.AXIAL;
        this._z = 0;
        this._invert = false;
        this._width = (int)this.vol.dim[0];
        this._height = (int)this.vol.dim[1];
        this._min_threshold = 0f;
        this._max_threshold = 1f;

        this._paused = false;
        this._pending = false;
        this._drawing = false;

        this.img = new Gtk.Image.from_pixbuf(new Pixbuf(Colorspace.RGB, false, 8, (int)this.vol.dim[0], (int)this.vol.dim[1]));

        this.add(this.img);
        this.add_events((int)EventMask.SCROLL_MASK);
        this.scroll_event.connect(scroll);
        this.size_allocate.connect(this.handle_size_allocate);

        this.redraw();
    }

    private bool scroll(EventScroll e)
    {
        if(
                ((e.direction & ScrollDirection.DOWN) == ScrollDirection.DOWN) ||
                ((e.direction & ScrollDirection.LEFT) == ScrollDirection.LEFT)
          )
        {
            this.z--;
        }
        else if(
                ((e.direction & ScrollDirection.UP) == ScrollDirection.UP) ||
                ((e.direction & ScrollDirection.RIGHT) == ScrollDirection.RIGHT)
               )
        {
            this.z++;
        }

        this.redraw();


        return false;
    }

    public static string scroll_dir_string(ScrollDirection d)
    {
        switch(d)
        {
            case ScrollDirection.UP:
                return "UP";
            case ScrollDirection.DOWN:
                return "DOWN";
            case ScrollDirection.LEFT:
                return "LEFT";
            case ScrollDirection.RIGHT:
                return "RIGHT";
            default:
                return "<UNKNOWN>";
        }
    }

    public void rotate(ViewRotation pos)
    {
        log_info("Rotation command: %s\n", ViewRotation.to_string(rot));

        int roti = (int)pos;

        if(roti < 3 && roti >= 0)
        {
            this._rot = rot;
        }
        else
        {
            roti += (int)this._rot;
            log_info("Converting from rotation \"%s\" to \"%s\"\n", ViewRotation.to_string(this.rot), ViewRotation.to_string((ViewRotation)roti));
            this._rot = (ViewRotation)(((int)pos + (int)this._rot) % 3);
        }

        this._z = 0; //force z to the first slice in the fixed direction

        this.redraw();
    }

    public void pause()
    {
        this._paused = true;
    }

    public void unpause()
    {
        this._paused = false;
        if(this._pending)
        {
            this.redraw();
        }
    }

    public void redraw()
    {
        if(this._drawing) return;
        if(this._paused)
        {
            this._pending = true;
            return;
        }
        else
        {
            this._pending = false;
        }

        this._drawing = true;

        Gdk.Pixbuf pixbuf = null;

        long idx = 0;
        uint8 pval = 0;
        uint8 *data = null;
        int stride = 0;
        float *vol_data = this.vol.data;
        float *origin = this.vol.offset;
        float *spacing = this.vol.spacing;
        long frame_width = 0;
        long frame_height = 0;
        float *xyz = malloc(sizeof(float) * 3);
        long *dim = this.vol.dim;
        float fval = 0f;

        switch(this._rot)
        {
            case ViewRotation.AXIAL:
                pixbuf = new Pixbuf(Colorspace.RGB, false, 8, (int)this.vol.dim[0], (int)this.vol.dim[1]);
                data = (uint8*)pixbuf.pixels;

                render_page(data, dim, vol_data, this._min_threshold, this._max_threshold, this._z);

                //stride = (int)this.vol.dim[0];
                //for(int x = 0; x < this.vol.dim[0]; x++)
                //{
                    //for(int y = 0; y < this.vol.dim[1]; y++)
                    //{
                        //idx = ((stride * y) + x) * 3;
                        //fval = this.vol.data[x,y,this._z];
                        //if(fval < this._min_threshold) fval = 0;
                        //if(fval > this._max_threshold) fval = 0;
                        //pval = (uint8)(fval * 255);
                        //data[idx + 0] = pval;
                        //data[idx + 1] = pval;
                        //data[idx + 2] = pval;
                    //}
                //}
                break;
            case ViewRotation.SAGITTAL:
                pixbuf = new Pixbuf(Colorspace.RGB, false, 8, (int)this.vol.dim[0], (int)this.vol.dim[2]);
                data = (uint8*)pixbuf.pixels;
                stride = (int)this.vol.dim[0];
                for(int x = 0; x < this.vol.dim[0]; x++)
                {
                    for(int y = 0; y < this.vol.dim[2]; y++)
                    {
                        idx = ((stride * y) + x) * 3;
                        pval = (uint8)(this.vol.data[x, this._z, y] * 255);
                        data[idx + 0] = pval;
                        data[idx + 1] = pval;
                        data[idx + 2] = pval;
                    }
                }
                break;
            case ViewRotation.CORONAL:
                pixbuf = new Pixbuf(Colorspace.RGB, false, 8, (int)this.vol.dim[2], (int)this.vol.dim[1]);
                data = (uint8*)pixbuf.pixels;
                stride = (int)this.vol.dim[2];
                for(int x = 0; x < this.vol.dim[2]; x++)
                {
                    for(int y = 0; y < this.vol.dim[1]; y++)
                    {
                        idx = ((stride * y) + x) * 3;
                        pval = (uint8)(this.vol.data[this._z, x, y] * 255);
                        data[idx + 0] = pval;
                        data[idx + 1] = pval;
                        data[idx + 2] = pval;
                    }
                }
                break;
            default:
                stderr.printf("Unrecognized rotation result.\n");
                break;
        }

        free(xyz);

        this.remove(this.img);

        this.img = new Gtk.Image.from_pixbuf(pixbuf);

        this.add(this.img);

        this.img.show_all();

        this._drawing = false;
    }

    public void handle_size_allocate(Allocation alloc)
    {
        this._width = alloc.width;
        this._height = alloc.height;
    }
}
