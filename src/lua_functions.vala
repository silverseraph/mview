using Lua;
using Readline;

[CCode(cname="handle_luainit")]
extern int handle_luainit(LuaVM l);

[CCode(cname="dotty")]
extern void dotty(LuaVM l);

void register_functions()
{
    /*register lua volumes and frames tables*/
    lua.new_table();
    lua.set_global("volumes");
    lua.new_table();
    lua.set_global("frames");

    /*register volume functions */
    lua.new_table();
    lua.push_string("open");
    lua.push_cfunction(volume_open);
    lua.raw_set(-3);
    lua.push_string("diff");
    lua.push_cfunction(volume_lua_diff);
    lua.raw_set(-3);
    lua.push_string("display");
    lua.push_cfunction(volume_display);
    lua.raw_set(-3);
    lua.push_string("list");
    lua.push_cfunction(volume_list);
    lua.raw_set(-3);
    lua.push_string("alias");
    lua.push_cfunction(volume_alias);
    lua.raw_set(-3);
    lua.set_global("volume");

    /*register frame functions */
    lua.new_table();
    lua.push_string("rotate");
    lua.push_cfunction(frame_rotate);
    lua.raw_set(-3);
    lua.push_string("list");
    lua.push_cfunction(frame_list);
    lua.raw_set(-3);
    lua.push_string("threshold");
    lua.push_cfunction(frame_threshold);
    lua.raw_set(-3);
    lua.push_string("invert");
    lua.push_cfunction(frame_invert);
    lua.raw_set(-3);
    lua.push_string("AXIAL");
    lua.push_integer((int)ViewRotation.AXIAL);
    lua.raw_set(-3);
    lua.push_string("CORONAL");
    lua.push_integer((int)ViewRotation.CORONAL);
    lua.raw_set(-3);
    lua.push_string("SAGITTAL");
    lua.push_integer((int)ViewRotation.SAGITTAL);
    lua.raw_set(-3);
    lua.push_string("CURRENT");
    lua.push_integer((int)ViewRotation.CURRENT);
    lua.raw_set(-3);
    lua.push_string("NEXT");
    lua.push_integer((int)ViewRotation.NEXT);
    lua.raw_set(-3);
    lua.push_string("LAST");
    lua.push_integer((int)ViewRotation.LAST);
    lua.raw_set(-3);
    lua.set_global("frame");

    lua.push_cfunction(lua_usleep);
    lua.set_global("usleep");
}

void push_volume(string path, VolumeData v)
{
    //get volume_table.  if it isn't a table, then create a table and set it there
    lua.get_global("volumes");
    if(lua.type(-1) != Lua.Type.TABLE)
    {
        lua.pop(-1);
        lua.new_table();
        lua.set_global("volumes");
        lua.get_global("volumes");
    }

    lua.push_string(path);
    lua.push_lightuserdata(v);

    lua.raw_set(-3);

    lua.set_global("volumes");
}

void push_frame(string name, MviewGroup g)
{
    lua.get_global("frames");
    if(lua.type(-1) != Lua.Type.TABLE)
    {
        lua.pop(-1);
        lua.new_table();
        lua.set_global("frames");
        lua.get_global("frames");
    }

    lua.push_string(name);
    lua.push_lightuserdata(g);
    lua.raw_set(-3);
    lua.set_global("frames");
}

int volume_open(LuaVM vm)
{
    if(vm.is_string(1))
    {
        string path = vm.to_string(1);
        if(!FileUtils.test(path, FileTest.EXISTS))
        {
            log_err("The provided path, \"%s\", does not exist\n", path);
            return 0;
        }

        string alias = path;

        if(vm.get_top() > 1) if(vm.is_string(2)) alias = vm.to_string(2);
        var v = new Volume.from_mha(path);
        var vd = new VolumeData.with_data(path, v, alias);
        push_volume(vd.alias, vd);
        cache[vd.path] = vd;
        cache[vd.alias] = vd;
    }
    else
    {
        log_err("The volume to be opened must be provided with a path.\n");
    }

    return 0;
}

int volume_display(LuaVM vm)
{
    if(vm.is_lightuserdata(1))
    {
        VolumeData vd = (VolumeData)vm.to_pointer(1);
        window.open_volume(vd.alias, vd.volume);
        window.show_all();
    }
    else if(vm.is_string(1))
    {
        string vol_name = vm.to_string(1);
        var vd = cache[vol_name];
        if(vd == null)
        {
            log_err("The volume, \"%s\", is not registered in the cache.\n", vol_name);
            return 0;
        }
        window.open_volume(vd.alias, vd.volume);
        window.show_all();
    }
    else
    {
        log_err("The volume to be opened must be indicated by either a string or a lightuserdata.\n");
    }

    return 0;
}

int volume_lua_diff(LuaVM vm)
{
    VolumeData a = null;
    VolumeData b = null;
    VolumeData c = null;

    if(vm.get_top() < 2)
    {
        log_err("Too few arguments for volume.diff(volume_a, volume_b, [volume_c])\n");
        return 0;
    }

    if(vm.is_lightuserdata(1)) a = (VolumeData)vm.to_pointer(1);
    else if(vm.is_string(1)) a = cache[vm.to_string(1)];
    else
    {
        log_err("Argument[1] for volume.diff must be a lightuerdata or a string\n");
        return 0;
    }

    if(vm.is_lightuserdata(2)) b = (VolumeData)vm.to_pointer(2);
    else if(vm.is_string(2)) b = cache[vm.to_string(2)];
    else
    {
        log_err("Argument[2] for volume.diff must be a lightuserdata or a string\n");
        return 0;
    }

    if(a == null)
    {
        log_err("Could not retrieve volume_a (argument[1]).\n");
        return 0;
    }

    if(b == null)
    {
        log_err("Could not retrieve volume_b (argument[2]).\n");
        return 0;
    }

    string path = "d(%s,%s)".printf(a.alias, b.alias);
    string alias = path;

    if(vm.get_top() > 2) if(vm.is_string(3)) alias = vm.to_string(3);

    Volume diff= a.volume.meta_clone();

    Volume.diff(diff, a.volume, b.volume);

    c = new VolumeData.with_data(path, diff, alias);

    cache[path] = c;
    cache[alias] = c;

    push_volume(alias, c);

    return 0;
}

int volume_alias(LuaVM vm) //volume.alias(volume, name)
{
    if(vm.get_top() < 2)
    {
        log_err("Too few arguments provided to volume.alias(volume, name)\n");
    }

    VolumeData vd = null;

    string new_alias = null;

    //VolumeData retrieval
    {
        if(vm.is_lightuserdata(1))
        {
            vd = (VolumeData)vm.to_pointer(1);
        }
        else if(vm.is_string(1))
        {
            vd = cache[vm.to_string(1)];

        }
        else
        {
            log_err("Argument[1] for volume.alias(volume, name) requires either a lightuserdata or a string.\n");
            return 0;
        }
    }

    //new_alias is determined
    {
        if(vm.is_string(2))
        {
            new_alias = vm.to_string(2);
        }
        else
        {
            log_err("Argument[2] for volume.alias(volume, name) requires a string.\n");
        }
    }

    string old_alias = vd.alias;

    //if the path was not duplicated into the alias name.  We don't want to wholly remove the volume after all
    if(old_alias != vd.path) 
    {
        //remove value from table
        cache.remove(old_alias);

        //remove value from volumes table in lua
        vm.get_global("volumes");
        if(vm.type(-1) != Lua.Type.TABLE)
        {
            lua.pop(-1);
            lua.new_table();
            lua.set_global("volumes");
            lua.get_global("volumes");
        }
        vm.push_string(old_alias);
        vm.push_nil();
        vm.raw_set(-3);
        vm.set_global("volumes");
    }

    cache[new_alias] = vd;
    //push new alias into the table
    push_volume(new_alias, vd);

    return 0;
}

int volume_list(LuaVM vm)
{
    lua.do_string("for k,v in pairs(volumes) do print(k) end ");
    string err = lua.to_string(-1);
    if(err != null) stderr.printf("ERROR: %s\n", err);
    vm.pop(-1);
    return 0;
}

int frame_rotate(LuaVM vm)
{
    if(vm.is_lightuserdata(1))
    {
        int rot_dir = (int)ViewRotation.NEXT;

        MviewGroup g = (MviewGroup)vm.to_pointer(1);

        if(vm.get_top() > 1)
        {
            if(vm.to_integer(2) >= 0 && vm.to_integer(2) < 6)
            {
                rot_dir = vm.to_integer(2);
            }
            else
            {
                log_err("Provided value for rotation must be an integer between 0 and 5, but received %d\n", vm.to_integer(2));
            }
        }

        g.page.rotate((ViewRotation)rot_dir);
    }
    else
    {
        log_err("The frame indicated does not exist.\n");
    }

    return 0;
}

int frame_invert(LuaVM vm)
{
    if(vm.is_lightuserdata(1))
    {
        MviewGroup g = (MviewGroup)vm.to_pointer(1);

        if(vm.get_top() > 1)
        {
            if(vm.is_boolean(2))
            {
                g.page.invert = (bool)vm.to_boolean(2);
            }
            else
            {
                log_err("Argument[2] must be a boolean.\n");
            }
        }
        else
        {
            g.page.invert = !g.page.invert;
        }
    }
    else
    {
        log_err("requires that the first argument be a lightuserdata.\n");
    }

    return 0;
}

int frame_list(LuaVM vm)
{
    lua.do_string("for k,v in pairs(frames) do print(k) end");
    string err = lua.to_string(-1);
    if(err != null) stderr.printf("ERROR: %s\n", err);
    vm.pop(-1);
    return 0;
}

int frame_threshold(LuaVM vm)
{
    MviewGroup g = null;
    float min = 0, max = 1;
    if(vm.get_top() > 2)
    {
        if(vm.is_lightuserdata(1))
        {
            g = (MviewGroup)vm.to_pointer(1);
        }
        else
        {
            log_err("Expected a lightuserdata for the first argument.\n");
            return 0;
        }

        if(vm.is_number(2) && vm.is_number(3))
        {
            min = (float)vm.to_number(2);
            max = (float)vm.to_number(3);

            g.page.pause();
            g.page.min_threshold = min;
            g.page.max_threshold = max;
            g.page.unpause();
        }
        else
        {
            log_err("Expected a number for the second and third arguments.\n");
        }
    }
    else
    {
        stdout.printf("frame.threshold requires three arguments\n");
    }
    return 0;
}

int lua_usleep(LuaVM vm)
{
    if(lua.is_number(1))
    {
        Thread.usleep(lua.to_integer(1) * 1000000);
    }
    else
    {
        log_err("Argument[1] to usleep must be a number\n");
    }

    return 0;
}
