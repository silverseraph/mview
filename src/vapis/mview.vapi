[PrintfFormat]
[CCode(cheader_filename="mview_utils.h", cname="log_err")]
public void log_err(string fmt, ...);

[PrintfFormat]
[CCode(cheader_filename="mview_utils.h", cname="log_warn")]
public void log_warn(string fmt, ...);

[PrintfFormat]
[CCode(cheader_filename="mview_utils.h", cname="log_info")]
public void log_info(string fmt, ...);

[PrintfFormat]
[CCode(cheader_filename="mview_utils.h", cname="log_nifty")]
public void log_nifty(string fmt, ...);

[CCode(cheader_filename="mview_utils.h", cname="debug_wait")]
public void debug_wait();

[CCode(cheader_filename="mview_utils.h", cname="mview_unused", simple_generics=true)]
public void unused<T>(T unused);
