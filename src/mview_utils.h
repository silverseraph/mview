#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>

#ifndef __mview_utils_header__
#define __mview_utils_header__

#define mview_unused(par) (void)par

extern bool DEBUG;

#ifndef true
#define true (1==1)
#endif

#ifndef false
#define false (!(true))
#endif

#ifndef NO_COLOR
#define MAGENTA "\033[1;31m"
#define ORANGE  "\033[1;33m"
#define GREEN   "\033[1;32m"
#define BLUE    "\033[1;34m"
#define PURPLE  "\033[1;35m"
#define WHITE   "\033[1;37m"
#define RESET   "\033[m"
#else
#define MAGENTA ""
#define ORANGE  ""
#define GREEN   ""
#define BLUE    ""
#define PURPLE  ""
#define WHITE   ""
#define RESET   ""
#endif

//definitions of errors as equilength so that logging isn't so fucked
#define ERROR "[ERROR]: "
#define WARN  "[WARN]:  "
#define INFO  "[INFO]:  "
#define NIFTY "[NIFTY]: "

//always show errors.  your face is about to melt off
#define log_err(M, ...)   fprintf(stderr, MAGENTA ERROR "(%s:%d:%s) " M RESET, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)

//always show warnings.  these are bad, but not terrible
#define log_warn(M, ...)  fprintf(stderr, ORANGE  WARN  "(%s:%d:%s) " M RESET, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)

//show interesting events always, use sparingly
#define log_nifty(M, ...) fprintf(stdout, PURPLE  NIFTY "(%s) "       M RESET,                     __FUNCTION__, ##__VA_ARGS__)

//show debug events, go crazy with this
#ifdef DEBUG
#define log_info(M, ...)  fprintf(stdout, GREEN   INFO  "(%s:%d:%s) " M RESET, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)
#define log_infos(M, ...) fprintf(stdout, GREEN   INFO  "(%s) "       M RESET,                     __FUNCTION__, ##__VA_ARGS__)
#else
#define log_info(M, ...)
#define log_infos(M, ...)
#endif

//add "wait" points for debugging only
#ifdef DEBUG
#define debug_wait() \
    do { \
        char __DEBUG_CHAR = 0; \
        do { \
            __DEBUG_CHAR = getchar(); \
        } while('\n' != __DEBUG_CHAR && EOF != __DEBUG_CHAR); \
    } while(0)
#else
#define debug_wait()
#endif

#define VOL_IDX(x, y, z, dim) (((x) * ((dim)[1]) * ((dim)[2])) + ((y) * ((dim)[2])) + (z))

#endif
