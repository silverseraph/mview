#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <omp.h>
#include <glib.h>

#include "mview_utils.h"

void volume_internal_diff(guint64 len, float *da, float *db, float *dr)
{
    guint64 i;
    log_info("Running diff <" PHI_STR " " OMP_STR ">\n");
#ifdef USE_PHI
    #pragma offload target(mic) \
        in(da,db:length(0) REUSE) \
        out(dr:length(len))
#endif
    {
#ifdef USE_OMP
        #pragma omp parallel for \
            default(none) \
            private(i) \
            shared(len, da, db, dr)
#endif
        for(i = 0; i < len; i++)
        {
            dr[i] = fabs(da[i] - db[i]);
        }
    }
}

#ifndef VOL_IDX
#define VOL_IDX(x, y, z, dim) ((z) + (((dim)[2]) * ((y) + (((dim)[1]) * (x) ))))
#endif

#if 0
float inline mview_page_volume_trilinear_interpolate(float *vol_data, float *origin, float *spacing, float *xyz, gint64 *dim, bool invert)
{
    float ridx[] = {0, 0, 0}; //ridx[i] = xyz[i] - origin[i]
    float rxyz[] = {0, 0, 0}; //rxyz[i] = ridx[i] / spacing[i]
    gint64 ldim[] = {0, 0, 0}; //ldim[i] = (gint64)floorf(rxyz[i])
    float deltas[] = {0, 0, 0, 0, 0, 0};
    float svox[] = {0, 0, 0, 0, 0, 0, 0, 0}; //surrounding voxels
    gint64 d;

    for(d = 0; d < 3; d++)
    {
        ridx[d] = xyz[d] - origin[d];
        rxyz[d] = ridx[d] / spacing[d];
        ldim[d] = (gint64)floorf(rxyz[d]);
        deltas[d] = (rxyz[d] - ldim[d]) * spacing[d];
        deltas[d+3] = ((ldim[d] + 1) - rxyz[d]) * spacing[d];
    }

    svox[0] = vol_data[VOL_IDX(ldim[0] + 0, ldim[1] + 0, ldim[2] + 0, dim)];
    svox[1] = vol_data[VOL_IDX(ldim[0] + 0, ldim[1] + 0, ldim[2] + 1, dim)];
    svox[2] = vol_data[VOL_IDX(ldim[0] + 0, ldim[1] + 1, ldim[2] + 0, dim)];
    svox[3] = vol_data[VOL_IDX(ldim[0] + 0, ldim[1] + 1, ldim[2] + 1, dim)];
    svox[4] = vol_data[VOL_IDX(ldim[0] + 1, ldim[1] + 0, ldim[2] + 0, dim)];
    svox[5] = vol_data[VOL_IDX(ldim[0] + 1, ldim[1] + 0, ldim[2] + 1, dim)];
    svox[6] = vol_data[VOL_IDX(ldim[0] + 1, ldim[1] + 1, ldim[2] + 0, dim)];
    svox[7] = vol_data[VOL_IDX(ldim[0] + 1, ldim[1] + 1, ldim[2] + 1, dim)];

    return 0;
}
#endif

float inline trilinear_interpolation(float *vol_data, float *origin, float *spacing, float *xyz, gint64 *dim, bool invert)
{
    /*
     * resolved xyz coordinates as if the origin was zero'd out.
     */
    float rxyz[] = {
                    (xyz[0] - origin[0]), 
                    (xyz[1] - origin[1]), 
                    (xyz[2] - origin[2])};

    /*
     * logical dimensions for the lower bounds of the volumes being indexed for interpolation
     */
    gint64 ldim[] = {(gint64)floorf(rxyz[0] / spacing[0]), 
                     (gint64)floorf(rxyz[1] / spacing[1]), 
                     (gint64)floorf(rxyz[2] / spacing[2])}; 

    /*
     * real space deltas between between the point and 
     * the upper and lower verticies surrounding the 
     * interpolated point
     */
    float delta[] = {
                      (rxyz[0] - (ldim[0] * spacing[0])),
                      (rxyz[1] - (ldim[1] * spacing[1])),
                      (rxyz[2] - (ldim[2] * spacing[2])),
                      (((ldim[0] + 1) * spacing[0]) - rxyz[0]),
                      (((ldim[0] + 1) * spacing[0]) - rxyz[0]),
                      (((ldim[0] + 1) * spacing[0]) - rxyz[0])}; 

   
    float val = ((vol_data[VOL_IDX(ldim[0] + 0, ldim[1] + 0, ldim[2] + 0, dim)] * (delta[0] * delta[1] * delta[2])) + 
                 (vol_data[VOL_IDX(ldim[0] + 0, ldim[1] + 0, ldim[2] + 1, dim)] * (delta[0] * delta[1] * delta[5])) +
                 (vol_data[VOL_IDX(ldim[0] + 0, ldim[1] + 1, ldim[2] + 0, dim)] * (delta[0] * delta[4] * delta[2])) +
                 (vol_data[VOL_IDX(ldim[0] + 0, ldim[1] + 1, ldim[2] + 1, dim)] * (delta[0] * delta[4] * delta[5])) +
                 (vol_data[VOL_IDX(ldim[0] + 1, ldim[1] + 0, ldim[2] + 0, dim)] * (delta[3] * delta[1] * delta[2])) +
                 (vol_data[VOL_IDX(ldim[0] + 1, ldim[1] + 0, ldim[2] + 1, dim)] * (delta[3] * delta[1] * delta[5])) +
                 (vol_data[VOL_IDX(ldim[0] + 1, ldim[1] + 1, ldim[2] + 0, dim)] * (delta[3] * delta[4] * delta[2])) +
                 (vol_data[VOL_IDX(ldim[0] + 1, ldim[1] + 1, ldim[2] + 1, dim)] * (delta[3] * delta[4] * delta[5])));


    if(invert) return (-val + 1);
    else       return val;
}

#ifndef PI
#define PI 3.1415926535898
#endif

static float _gauss(float dx, float dy, float dz, float sigma)
{
    return expf(-((dx*dx + dy*dy + dz*dz) / (sigma*sigma))) * (1/(powf(sqrtf(2*PI) * sigma, 3)));
}

static float *_gauss_gen(float sigma, long int dim)
{  
    int i, j, k, idx = 0; 
    float *data = (float*)malloc(sizeof(float) * dim * dim * dim);

    #pragma omp simd collapse(3) \
        private(idx)
    for(k = 0; k < dim; k++)
    {
        for(j = 0; j < dim; j++)
        {
            for(i = 0; i < dim; i++)
            {
                idx = (k * dim * dim) + (j * dim) + i;
                data[idx] = _gauss(
                                fabs((float)(i - (dim/2))), 
                                fabs((float)(j - (dim/2))), 
                                fabs((float)(k - (dim/2))), 
                                sigma);
            }
        }
    }

    return data;
}

void gauss_filter(float *odata, float *idata, long int *dim, long int filter_dim, float sigma)
{
    float *filter = _gauss_gen(sigma, filter_dim);

    long int hfd = filter_dim/2;

    long int x, y, z, a, b, c;

    long int x_std = filter_dim * filter_dim, y_std = filter_dim;

    #pragma omp parallel for \
        default(none) \
        private(x, y, z, a, b, c) \
        shared(odata, idata, dim, filter, sigma, hfd, filter_dim, x_std, y_std) \
        collapse(3)
    for(x = hfd; x < dim[0]-hfd; x++)
    {
        for(y = hfd; y < dim[1]-hfd; y++)
        {
            for(z = hfd; z < dim[2]-hfd; z++)
            {
                long int i_idx = (x * x_std) + (y * y_std) + z;

                float cell_val = 0;

                #pragma omp simd \
                    collapse(3) \
                    reduction(+:cell_val)
                for(a = 0; a < filter_dim; a++)
                {
                    for(b = 0; b < filter_dim; b++)
                    {
                        for(c = 0; c < filter_dim; c++)
                        {
                            long int f_idx = (a * filter_dim * filter_dim) + (b * filter_dim) + c;

                            cell_val += filter[f_idx] * idata[i_idx + ((a-2) * x_std) + ((b-2) * y_std) + (z-2)];
                        }
                    }
                }

                odata[i_idx] = cell_val;
            }
        }
    }

    free(filter);
}

static const float sobel_x[3][3][3] =
{
    { 
        { -1,  0,  1},
        { -3,  0,  3},
        { -1,  0,  1}
    },
    {
        { -3,  0,  3},
        { -6,  0,  6},
        { -3,  0,  3}
    },
    {
        { -1,  0,  1},
        { -3,  0,  3},
        { -1,  0,  1}
    }
};

static const float sobel_y[3][3][3] = 
{
    {
        { -1, -3, -1},
        {  0,  0,  0},
        { -1, -3, -1}
    },
    {
        { -3, -6, -3},
        {  0,  0,  0},
        { -3, -6, -3}
    },
    {
        { -1, -3, -1},
        {  0,  0,  0},
        {  1,  3,  1}
    }
};

static const float sobel_z[3][3][3] = 
{
    {
        { -1, -3, -1},
        { -3, -6, -3},
        { -1, -3, -1}
    },
    {
        {  0,  0,  0},
        {  0,  0,  0},
        {  0,  0,  0}
    },
    {
        {  1,  3,  1},
        {  3,  6,  3},
        {  1,  3,  1}
    }
};

#if 0
void sobel_filter(float *odata, float *idata, long int *dim)
{
    long int idx, lim = dim[0] * dim[1] * dim[2];

    long int std[3] = {dim[1] * dim[2], dim[2], 1};

    #pragma omp parallel for 
    for(idx = 0; idx < lim; idx++)
    {
        float sum = 0;

        #pragma omp simd collapse(3)
        for(int x = 0; x < 3; x++)
        {
            for(int y = 0; y < 3; y++)
            {
                for(int z = 0; z < 3; z++)
                {
                    
                }
            }
        }
    }
}
#endif
