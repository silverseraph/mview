public enum ViewRotation
{
    AXIAL = 0,
    CORONAL = 1,
    SAGITTAL = 2,
    CURRENT = 3,
    NEXT = 4,
    LAST = 5;

    public static string to_string(ViewRotation v)
    {
        switch(v)
        {
            case ViewRotation.AXIAL:
                return "AXIAL";
            case ViewRotation.CORONAL:
                return "CORONAL";
            case ViewRotation.SAGITTAL:
                return "SAGITTAL";
            case ViewRotation.CURRENT:
                return "CURRENT";
            case ViewRotation.NEXT:
                return "NEXT";
            case ViewRotation.LAST:
                return "LAST";
            default:
                return "<UNKNOWN>";
        }
    }
}
